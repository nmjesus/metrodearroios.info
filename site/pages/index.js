import React, { useRef, useEffect } from 'react';
import moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';

moment.locale('pt');

function drawLine(canvas, ctx) {
  ctx.beginPath();
  ctx.moveTo(0, 20);
  ctx.lineWidth = 10;
  ctx.lineTo(canvas.offsetWidth, 20);
  ctx.strokeStyle = "#00a9a7";
  ctx.stroke();
}

function drawStops(canvas, ctx) {
  const stops = 11;
  const arroios = 5;
  const spacing = (canvas.offsetWidth / stops);

  for (let i = 0; i <= stops; i++) {
    ctx.beginPath();

    if (i === 0) {
      ctx.arc(10, 20, 3, 0, 2 * Math.PI);
    } else if (i === stops) {
      ctx.arc(canvas.offsetWidth-10, 20, 3, 0, 2 * Math.PI);
    } else {
      ctx.arc(spacing * i, 20, 3, 0, 2 * Math.PI);
    }

    if (i !== arroios) {
      ctx.fillStyle = "white";
    } else {
      ctx.fillStyle = "red";
    }

    ctx.strokeStyle = "white";
    ctx.fill();
    ctx.stroke();
  }
}

const useStyles = makeStyles({
  root: {
    width: '100%',
    maxWidth: '100%',
    padding: '0',
    height: '100vh',
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  h1: {
    textAlign: 'center',
    fontFamily: 'Ubuntu, sans-serif',
    color: '#fff',
  },
  canvas: {
    width: '80vw',
    height: '50px',
  }
});

function Index({ since }) {
  const classes = useStyles();
  const canvasRef = useRef();

  useEffect(() => {
    const canvas = canvasRef.current;
    const ctx = canvas.getContext('2d');
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
    drawLine(canvas, ctx);
    drawStops(canvas, ctx);
  }, [canvasRef.current]);

  return (
    <Container classes={{ root: classes.root }}>
      <Typography variant="h1" classes={{ root: classes.h1 }}>
        <p>Metro de arroios está fechado</p>
        <p>{since}</p>
      </Typography>
      <canvas ref={canvasRef} className={classes.canvas}></canvas>
    </Container>
  );
}

Index.getInitialProps = (req, res) => {
  return {
    since: moment("20170719", "YYYYMMDD").fromNow(),
  }
}

export default Index
